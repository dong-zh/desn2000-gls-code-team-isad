# DESN2000 Team ISAD 😢

## Links

### [Excel Data](https://unsw-my.sharepoint.com/:x:/g/personal/z5257526_ad_unsw_edu_au/ETUgKPLfME9Kux3GO4zRxAoBOuUBf-rgrZRVexE_9e64fg)

### [This Repository](https://gitlab.com/dong-zh/desn2000-gls-code-team-isad)

## Arduino Files

### `gls.ino`

- Code for our prototype
- It optionally stores the collected data into its EEPROM memory
- It keeps a counter that updates the first 2 bytes of the EEPROM every write, which is very bad for the EEPROM as it has very limited life cycles.
- After the first 2 bytes for the counter, the `struct` looks like this:

    ```C
    struct EepromEntry {
        uint32_t time;
        float temperature;
        int32_t pressure;
        uint16_t ldrValue;
        uint16_t photodiodeValue
    };
    ```

### `eepromRead.ino`

- A program to read the above data from the EEPROM and print it out in the serial monitor
- Optional CSV mode for easy data processing

### `lightTestBench.ino`

- Code for the light test bench/generator/whatever you want to call it
- Also features a CSV mode for easy data processing

## ARM Files

### `gls.c`

The actual code for the GLS targeting the LPC2478. Features:

- A global clock to keep track of how many seconds since startup
- A generic delay function that's used to sample every 5 minutes
- Samples 2 analogue sensors from the ADC (for the photodiode and light dependent resistor)
- Stores all the data in this struct

    ```C
    struct DataEntry {
        uint32_t time;
        struct BmpData bmpData;
        uint16_t adc0;
        uint16_t adc1;
    };
    ```

- Data is saved in an array in memory for now
- `MAX_ENTRIES` has been defined to 100 as an example. We hope to store this data in SD card, so there might be some fine tuning regarding buffering to be done.

### `I2C_protocol.c`

A WIP of how I2C can be used to communicate with the BMP180.

## `cellularCapability.pdf`

An estimation on how feasible it is to include an LTE modem into our design

## `formula.c`

Calculations for sunrise, sunset, longitude, and latitude.

## `formula_concept.c`

An idea of how to get the longitude and latitude from the sunrise and sunset. This is not yet complete.
