#include <stdio.h>
#include <math.h>

#define UploadTime              12
#define Sensor                  A3
#define Button                   4
#define N                       15      // Filter size (=2*N+1)

const float Altitude        = -2.50;    // sunrise/sunset inflection point
const float LocalTimeOffset = 8.000;    // Perth WA


float J2000(long Year, long Month, long Day) {
    float JD;
    
    // Julian (2000) Day Number
    JD = (float)(367 * Year - (7 * (Year + (Month + 9) / 12)) / 4 + 275 * Month / 9 + Day);
    JD = JD - 730531.5;
    
    return JD;
}

float GMST(float J2000, float UT) {
    float gmst;
    
    // Greenwich Mean Sidereal Time
    gmst = 0.06570982441908 * (J2000 - 0.5);
    gmst = fmod(gmst, 24.0);
    gmst = gmst + 6.73022947 + 1.002737851 * UT;
    gmst = fmod(gmst + 24.0, 24.0);
    
    return gmst;
}

void SunData(float J2000, float UT, float *RA, float *Dec) {
  float L,g,a,c;
  
  /* Source: Page C24 from "The Astronomical Almanac" (1986) */
  // Mean longitude of Sun, corrected for aberration
    L = 0.9856474 * J2000;
    L = fmod(L, 360.0);
    L = L + 280.460 + 0.9856474 * UT / 24.0;
    L = fmod(L + 360.0,360.0);
        
    // Mean anomaly
    g = 0.9856003 * J2000;
    g = fmod(g, 360.0);
    g = g + 357.528 + 0.9856003 * UT / 24.0;
    g = fmod(g + 360.0, 360.0);
    
    // Ecliptic longitude
    a = L + 1.915 * sin(g * M_PI / 180) + 0.020 * sin(2 * g * M_PI / 180);
    a = fmod(a, 360.0);
    // Ecliptic Latitude
    // float b=0;
    
    // Obliquity of ecliptic
    c = -0.0000004 * J2000;
    c = fmod(c, 360.0);
    c = c + 23.439 - 0.0000004 * UT / 24.0;
    c = fmod(c + 360.0,360.0);
    
    // Right ascension
    (*RA) = degrees(atan2(cos(radians(c)) * sin(radians(a)), cos(radians(a)))) / 15.0;
    if ((*RA) < 0 ) {
        (*RA) = (*RA) + 24.0;
    }
    // Declination
    (*Dec) = degrees(asin(sin(radians(c)) * sin(radians(a))));
}

float DayLight(float altitude) {
    if (altitude >= 0) {
        return (((0.002853 * altitude - 0.7399) * altitude + 53.16) * altitude + 548.6) * altitude + 759;
    }

    else if (altitude >= -3.0) {
        return (((-0.4469 * altitude + 3.098) * altitude + 82.24) * altitude + 433.4) * altitude + 759;
    }
    
    else {
        return 1588.8 * exp(altitude);
    }
}

float Latitude(float sunrise, float sunset, float altitude, float Dec) {
    float A,B,C,D,U,V,latitude;

    A = tan(radians(Dec));
    B = cos(radians(7.5 * (sunset - sunrise)));
    C = sin(radians(altitude)) / cos(radians(Dec));
    D = sqrt(A * A + B * B);
    if (fabs(C) <= D) {
        U = degrees(asin(B / D));
        V = degrees(asin(C / D));
        latitude = V;
        if (sunset>sunrise) {
            latitude = V - U;
        }
        if (sunset < sunrise) {
            latitude = V + U;
        }
        if (Dec<0) {
            latitude = -latitude;
        }
    } 
    
    else {
        latitude = 0.0;
    }

    return latitude;
}
