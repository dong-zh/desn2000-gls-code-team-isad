
/**
 * Purpose: proof of concept for DESN2000 GLS sensor
 * Date: 21T2
 * Programmer: Dongzhu Huang
 **/

#include <Adafruit_BusIO_Register.h>
#include <Adafruit_I2CDevice.h>
#include <Adafruit_I2CRegister.h>
#include <Adafruit_SPIDevice.h>
#include <Adafruit_BMP085.h>
#include <RTClib.h>
#include <EEPROM.h>


#define PRETTY_PRINT true
#define WRITE_TO_EEPROM false
#define LOOP_WAIT_TIME 5000
#define HALT_WHEN_EEPROM_FULL true

#define DEBUG true
#define PHOTODIODE_PIN A1
#define LDR_PIN A0
#define ENABLE_PIN A3
#define LED_PIN 2
#define BAUD_RATE 9600
#define CALIBRATE_RTC false
#define RTC_OFFSET_SECONDS 10

// Globals
// bmp sensor
Adafruit_BMP085 bmp;
// Buffer for printing
char buffer[BUFSIZ / 2];
// RTC
RTC_DS1307 rtc;
// EEPROM pointer
unsigned eepromPointer = 0;

struct SensorEntry {
	DateTime time;
	float temperature;
	float altitude;
	int32_t pressure;
	uint16_t ldrValue;
	uint16_t photodiodeValue;
};

struct EepromEntry {
	uint32_t time;
	float temperature;
	int32_t pressure;
	uint16_t ldrValue;
	uint16_t photodiodeValue;
};

void setup() {
	pinMode(LED_PIN, OUTPUT);
	pinMode(ENABLE_PIN, INPUT);
	pinMode(PHOTODIODE_PIN, INPUT);
	pinMode(LDR_PIN, INPUT);

	Serial.begin(BAUD_RATE);

	if (!digitalRead(ENABLE_PIN)) {
		HCF(100, 100);
	}

	initBmp();
	initRtc();

	if (WRITE_TO_EEPROM) {
		initEeprom();
	}
}

void loop() {
	if (!PRETTY_PRINT) {
		Serial.println("DATA START");
		Serial.println("TIME,TEMP,PRES,LDRV,PHOTODIODE");
	}

	while (checkEnable()) {
		struct SensorEntry data = getData();
		outputData(data);

		if (WRITE_TO_EEPROM) {
			writeToEeprom(data);
		}

		delay(LOOP_WAIT_TIME);
	}

	Serial.println("Exited from main loop");
	HCF(100, 100);
}

boolean checkEnable() {
	return digitalRead(ENABLE_PIN);
}

void writeToEeprom(SensorEntry data) {
	static boolean eepromFull;
	static unsigned entries;

	if (DEBUG) {
		Serial.print("Write to EEPROM called, EEPROM counter is ");
		Serial.print(entries);
		Serial.print(", EEPROM pointer is ");
		Serial.println(eepromPointer);
	}

	if (eepromFull) {
		if (DEBUG) {
			Serial.println("EEPROM is full");
		}
		return;
	}
	entries++;

	EEPROM.put(eepromPointer, populateEntry(data));
	EEPROM.put(0, entries);

	eepromPointer += sizeof(EepromEntry);

	if (DEBUG) {
		Serial.println("EEPROM write complete");
	}

	if (eepromPointer + sizeof(EepromEntry) > EEPROM.length()) {
		eepromFull = true;
		if (DEBUG) {
			Serial.println("No more room, EEPROM full");
			sprintf(buffer, "entries: %d, pointer: %d", entries, eepromPointer);
			Serial.println(buffer);
		}
		if (HALT_WHEN_EEPROM_FULL) {
			HCF(10, 990);
		}
	}

}

EepromEntry populateEntry(SensorEntry data) {
	struct EepromEntry entry;
	entry.ldrValue = data.ldrValue;
	entry.pressure = data.pressure;
	entry.temperature = data.temperature;
	entry.time = data.time.unixtime();
	entry.photodiodeValue = data.photodiodeValue;

	return entry;
}

void initEeprom() {
	EEPROM.put(0, 0);
	eepromPointer += sizeof(0);
	Serial.println("EEPROM init write success");
}

struct SensorEntry getData() {
	struct SensorEntry data;
	data.altitude = bmp.readAltitude();
	data.pressure = bmp.readPressure();
	data.ldrValue = analogRead(LDR_PIN);
	data.temperature = bmp.readTemperature();
	data.time = rtc.now();
	data.photodiodeValue = analogRead(PHOTODIODE_PIN);

	return data;
}

void initRtc() {
	if (!rtc.begin()) {
		Serial.println("Failed to activate RTC");
		HCF(500, 500);
	}

	Serial.println("RTC init success");

	if (!rtc.isrunning()) {
		Serial.println("RTC isn't running");
	}

	if (CALIBRATE_RTC || !rtc.isrunning()) {
		calibrateRtc();
	}

	Serial.println("RTC is running");

}

void calibrateRtc() {
	rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
	DateTime now = rtc.now();
	rtc.adjust(now.unixtime() + RTC_OFFSET_SECONDS);
	Serial.println("RTC calibrated");
}

void initBmp() {
	if (!bmp.begin()) {
		Serial.println("Failed to active BMP sensor");
		HCF(500, 500);
	}

	Serial.println("BMP sensor init success");
}

void prettyPrint(SensorEntry data) {
	strcpy(buffer, "YYYY-MM-DDThh:mm:ss");
	Serial.println(data.time.toString(buffer));

	Serial.print("\tTemperature:\t");
	Serial.print(data.temperature);
	Serial.println(" Celsius");

	Serial.print("\tAltitude:\t\t");
	Serial.print(data.altitude);
	Serial.println(" metres");

	Serial.print("\tPressure\t\t");
	Serial.print(data.pressure);
	Serial.println(" pascals");

	sprintf(buffer, "\tLDR value:\t\t%d\n", data.ldrValue);
	Serial.print(buffer);
	sprintf(buffer,"\tP. diode value: \t\t %d\n", data.photodiodeValue);
	Serial.print(buffer);
}

void csvPrint(SensorEntry data) {
	Serial.print(data.time.unixtime());
	Serial.print(',');
	Serial.print(data.temperature);
	Serial.print(',');
	Serial.print(data.altitude);
	Serial.print(',');
	Serial.print(data.ldrValue);
	Serial.print(',');
	Serial.print(data.photodiodeValue);
	Serial.println();
}

void outputData(SensorEntry data) {
	if (PRETTY_PRINT) {
		prettyPrint(data);
	} else {
		csvPrint(data);
	}
}

/**
 * Halts and catches fire (not really, just halts and blinks an LED)
 *
 * @param onTime How many milliseconds the LED stays on for
 * @param offTime How many milliseconds the LED stays off for
 */
void HCF(unsigned onTime, unsigned offTime) {
	Serial.println("HALTING");

	while (true) {
		digitalWrite(LED_PIN, HIGH);
		delay(onTime);
		digitalWrite(LED_PIN, LOW);
		delay(offTime);
	}
}
