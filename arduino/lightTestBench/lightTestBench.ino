/**
 * Purpose: Test bench for the light sensor for DESN2000
 * Date: 21T2
 * Programmer: Dongzhu Huang
 */

#define CSV_MODE true
#define WAIT_TIME 1000 * 10
#define SAMPLE_WAIT_TIME 50
#define BRIGHTNESS_STEPS 10
#define LOOP false

#define DEBUG true
#define BAUD_RATE 9600
#define SENSOR_PIN A0
#define LED1_PIN 3
#define ARRAY_LENGTH (WAIT_TIME / SAMPLE_WAIT_TIME + 1)

char buffer[BUFSIZ / 2];

void setup() {
	Serial.begin(BAUD_RATE);
	pinMode(LED1_PIN, OUTPUT);
	pinMode(SENSOR_PIN, INPUT);
}

void loop() {
	if (CSV_MODE) {
		Serial.println("PWM VALUE,PHOTODIODE");
	}
	do {
		// Very low light
		for (int i = 0; i < 10; i++) {
			doSample(i);
		}

		// Low to medium light
		for (int i = 1; i <= BRIGHTNESS_STEPS; i++) {
			unsigned pwmValue = round((double)i / (double)BRIGHTNESS_STEPS * (double)UINT8_MAX);
			doSample(pwmValue);
		}

	} while (LOOP);

	HCF(0, ~0l);
}

void doSample(const unsigned pwmValue) {
	analogWrite(LED1_PIN, pwmValue);
	unsigned values[ARRAY_LENGTH];
	unsigned samples = 0;
	unsigned long start = millis();
	for (unsigned cycle = 0; millis() < start + WAIT_TIME; cycle++) { // Change LED brightness every WAIT_TIME
		values[samples++] = analogRead(SENSOR_PIN);

		while (millis() < start + cycle * SAMPLE_WAIT_TIME) {} // Sample every SAMPLE_WAIT_TIME
	}

	if (CSV_MODE) {
		csvPrint(pwmValue, average((int *)values, samples));
	} else {
		prettyPrint(pwmValue, average((int *)values, samples));
	}
}

void prettyPrint(const unsigned pwmValue, const double average) {
	sprintf(buffer, "PWM value %d (%d%%)\n\tAverage: ", pwmValue, getPercent(pwmValue, UINT8_MAX));
	Serial.print(buffer);
	Serial.println(average);
}

void csvPrint(const unsigned pwmValue, const double average) {
	Serial.print(pwmValue);
	Serial.print(',');
	Serial.print(average);
	Serial.println();
}

/**
 * Converts a fraction to a percentage
 *
 * @param numerator The numerator
 * @param denominator The denominator
 * @returns An integer between [-100, 100]
 */
int getPercent(const double numerator, const double denominator) {
	return round((numerator / denominator) * 100);
}

/**
 * Returns the average of an array of integers, will probably break for 0 length
 *
 * @param array The array of integers
 * @param length The length of the array
 * @returns The average of the array
 */
double average(const int *array, const unsigned length) {
	long sum = 0;
	for (unsigned i = 0; i < length; i++) {
		sum += array[i];
	}

	return (double)sum / (double)length;
}

/**
 * Halts and catches fire (not really, just halts and blinks an LED)
 *
 * @param onTime How many milliseconds the LED stays on for
 * @param offTime How many milliseconds the LED stays off for
 */
void HCF(const unsigned onTime, const unsigned offTime) {
	Serial.println("HALTING");

	while (true) {
		digitalWrite(LED1_PIN, HIGH);
		delay(onTime);
		digitalWrite(LED1_PIN, LOW);
		delay(offTime);
	}
}
