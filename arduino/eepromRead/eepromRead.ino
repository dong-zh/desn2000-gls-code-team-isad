#include <EEPROM.h>

#define BAUD_RATE 9600
#define CSV_MODE true

#define LED_PIN 2

struct EepromEntry {
	uint32_t time;
	float temperature;
	int32_t pressure;
	uint16_t ldrValue;
	uint16_t photodiodeValue;
};

void setup() {
	Serial.begin(BAUD_RATE);
	pinMode(2, OUTPUT);

	if (!CSV_MODE) {
		Serial.println("Waiting 1 second...");
	}
	delay(1000);
}

void loop() {
	unsigned eepromPointer = 0;

	unsigned length;
	EEPROM.get(0, length);
	eepromPointer += sizeof(length);

	if (!CSV_MODE) {
		Serial.print("Length: ");
		Serial.println(length);
	}

	EepromEntry *entries = (EepromEntry *)calloc(length, sizeof(EepromEntry));

	Serial.println("UNIX TIME,TEMPERATURE,PRESSURE,P DIODE,LDR VALUE");
	for (int i = 0; i < length && eepromPointer + sizeof(EepromEntry) <= EEPROM.length(); i++, eepromPointer += sizeof(EepromEntry)) {
		EepromEntry currentEntry;
		EEPROM.get(eepromPointer, currentEntry);
		entries[i] = currentEntry;

		if (!CSV_MODE) {
			Serial.print(i);
			Serial.print('\t');
		}

		Serial.print(currentEntry.time);
		Serial.print(',');
		Serial.print(currentEntry.temperature);
		Serial.print(',');
		Serial.print(currentEntry.pressure);
		Serial.print(',');
		Serial.print(currentEntry.photodiodeValue);
		Serial.print(',');
		Serial.println(currentEntry.ldrValue);
	}

	HCF(0, ~0l);
}

/**
 * Halts and catches fire (not really, just halts and blinks an LED)
 *
 * @param onTime How many milliseconds the LED stays on for
 * @param offTime How many milliseconds the LED stays off for
 */
void HCF(unsigned onTime, unsigned offTime) {
	if (!CSV_MODE) {
		Serial.println("HALTING");
	}

	while (true) {
		digitalWrite(LED_PIN, HIGH);
		delay(onTime);
		digitalWrite(LED_PIN, LOW);
		delay(offTime);
	}

	delay(~0l);
}
