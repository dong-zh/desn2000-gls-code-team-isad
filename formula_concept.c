//Determine the Equations of Time (EqT) in minutes of time
//This is used in the calculation of longitude to correct for the elliptical nature of the earth's orbit, and the tilt of the earth's axis, in the following manner:
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

int main(void) {
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    printf("now: %d-%02d-%02d %02d:%02d:%02d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    // lines above are to present the local timestamp.
    int local_year = tm.tm_year + 1900;
    int local_month = tm.tm_mon + 1;
    int local_day = tm.tm_mday;
    int local_hour = tm.tm_hour;
    int local_minute = tm.tm_min;
    int local_second = tm.tm_sec;
    int correctedyear;
    int correctedmonth;
    double G;
    double C;
    double I;
    double alpha;
    double T;
    double EqT;
    double east_longitude;
    double time_midnight;

    if (local_month > 2) {
        correctedyear = local_year;
        correctedmonth = local_month - 3;
    }
    else {
        correctedyear = local_year - 1;
        correctedmonth = local_month + 9;
    }
    //T = (MidnightGMT - Int(MidnightGMT) + Day(MidnightGMT) Not sure how to convert this.
    T = T + (int)(30.6 * correctedmonth + 0.5);
    T = T + (int)(365.25 * (correctedyear - 1976)) - 8707.5) / 36525);

    G = (357.528 + 35999.05 * (T));
    C = 1.915 * sin((M_PI/180)* (G)) + 0.02 * sin((M_PI/180) * (G * 2));
    I = (280.46 + C + 36000.77 * T);
    alpha = 2.466 * sin((M_PI/180) * (2 * I)) - 0.053 * sin((M_PI/180) * (4 * I));
    EqT = (alpha - C) * 4;

    east_longitude = 360-(/*time of midnight in hours*/)*15-(EqT)/4; //Not sure how to convert this.

    return east_longitude;
}
