#include "lpc_24xx.h"

#define AA 2   // Assert acknoledge
#define SI 3   // I2C interrupt
#define STO 4  // Stop
#define STA 5  // Start
#define I2EN 6 // Interface Enable
/*
#define AAC 2   // Assert acknoledge clear
#define SIC 3   // I2C interrupt clear
#define STAC 5  // Start clear
#define I2ENC 6 // Interface Disable
*/
void wait(unsigned int delay) {
    while (delay--);
}

void i2c_init(void) {                   // I2C initilaization
    I20SCLH = 100;                      // Bit frequency calculated value
    I20SCLL = 100;
    I20CONSET = 1 << I2EN;
}

int i2c_start() {                       // I2C communication start
    I20CONCLR = 1 << 3;
    I20CONSET = 1 << STA;
    while (!(I20STAT == 0x08));
    return 0;
}

int i2c_write(unsigned char buff) {     // Data writing through I2C
    I20CONSET |= 1 << SI;
    I20DAT = buff;
    wait(5000);
    I20CONCLR = 1 << SI;
    wait(5000);
    return 0;
}

void i2c_stop(void) {                   // Stop the I2C communication
    I20CONSET = (1 << STO);
    I20CONCLR = (1 << I2EN);
}

int main(void) {
    VPBDIV = 0x02;                      // Setting FCLK frequency
    PINSEL0 = (1 << 4)|(1 << 6);        // Selecting I2C communication pins
    i2c_init();
    i2c_start();
    i2c_write(0x00);
    i2c_write('A');
    i2c_write('B');
    i2c_write('C');
    i2c_stop();
    while (1);
}
