/**
 * @file gls.c
 *
 * @author Team ISAD 😢
 * @author Indi Warner (z5310423)
 * @author Sabine Seeto (z5312137)
 * @author Anderson Tsai (z5237541)
 * @author Dongzhu Huang (z5257526)
 *
 * @brief DESN2000 Code for GLS targeting LPC2478
 * @date 21T2
 */

#include "lpc_24xx.h"
#include <ctype.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

// How many entries we want to store
#define MAX_ENTRIES 100

// Whether to halt when the array is full
#define HALT_IF_FULL true

// An entry from the BMP sensor
struct BmpData {
	unsigned pressure;
	float temperature;
};

// An entry for one sample with two ADC connected
struct DataEntry {
	uint32_t time;
	struct BmpData bmpData;
	uint16_t adc0;
	uint16_t adc1;
};

// Function prototypes
void initialiseAdc(void);
void waitMilli(unsigned);
struct BmpData readBmp(void);
uint16_t readAdc0(void);
uint16_t readAdc1(void);
bool saveData(struct DataEntry);
void halt(void);
uint32_t getSecondsSinceStart(void);
void startGlobalTimer(void);

int main(void)
{
	// Samples every 5 minutes
	const unsigned millisecondsPerSample = 300000;
	bool storageSuccessful = true;

	// TODO maybe add interrupt RTC logic instead?
	initialiseAdc();
	startGlobalTimer();

	// Continue looping unless HALT_IF_TRUE is set and storage failed
	do {
		struct DataEntry entry;
		entry.adc0 = readAdc0();
		entry.adc1 = readAdc1();
		entry.bmpData = readBmp();
		entry.time = getSecondsSinceStart();

		storageSuccessful = saveData(entry);
		waitMilli(millisecondsPerSample);
	} while (!(HALT_IF_FULL && !storageSuccessful));

	halt();
}

/**
 * @brief Starts the global timer, it should tick for 100 years, this is used to keep track of time since start. Uses timer 0.
 */
void startGlobalTimer(void)
{
	const uint32_t prSeconds = 71999999;
	// Stop timer and set TCR[0] when timer reaches MR
	// This shouldn't happen for at least 100 years
	T0MCR = 0x4;
	// Reset timer
	T0TCR = 0x2;
	// Count for over 100 years
	T0MR0 = 0xffffffff;
	// Tick every second
	T0PR = prSeconds;
	// Start the timer
	T0TCR = 0x1;
}

/**
 * @brief Gets time since start.
 *
 * @return How many seconds since start
 */
uint32_t getSecondsSinceStart(void)
{
	return T0TC;
}

/**
 * @brief Waits 'millisecondsToWait' milliseconds. Uses timer 1.
 *
 * @param millisecondsToWait How many seconds to wait
 */
void waitMilli(unsigned millisecondsToWait)
{
	const uint32_t prSeconds = 71999;
	// Stop timer and set TCR[0] when timer reaches MR
	T1MCR = 0x4;
	// Reset timer
	T1TCR = 0x2;
	// Send end time
	T1MR0 = millisecondsToWait;
	// Tick every millisecond
	T1PR = prSeconds;
	// Start the timer
	T1TCR = 0x1;

	// Wait until timer finishes
	while ((T1TCR & 0x1) == 1) {}
}

/**
 * @brief Reads data from the BMP180 via I2C, just a stub that returns an empty struct for now.
 *
 * @return A struct with the relevant data from the BMP180
 */
struct BmpData readBmp(void)
{
	// TODO interface with the sensor via I2C
	struct BmpData data;
	return data;
}

/**
 * @brief Reads an entry from the ADC (AD0). Turns on ADC -> reads data -> turns off ADC.
 *
 * @return The value read from the ADC (AD0)
 */
uint16_t readAdc0(void)
{
	const uint32_t sel = 0;
	const uint32_t clkDiv = 15;
	uint32_t pdn = 1;
	uint32_t start = 1;

	const uint32_t doneMask = 1u << 31;
	const uint32_t dataMask = 0x3ff << 6;

	uint16_t answer = 0;

	// Starts the ADC
	AD0CR = sel | (clkDiv << 8) | (pdn << 21) | (start << 24);

	// Wait until done bit is set
	while (!(AD0DR0 & doneMask)) {}

	// Actually reads the data
	answer = (AD0DR0 & dataMask) >> 6;

	// Shuts down the ADC
	pdn = 0;
	start = 0;
	AD0CR = sel | (clkDiv << 8) | (pdn << 21) | (start << 24);

	return answer;
}

/**
 * @brief Reads an entry from the ADC (AD1). Turns on ADC -> reads data -> turns off ADC.
 *
 * @return The value read from the ADC (AD1)
 */
uint16_t readAdc1(void)
{
	const uint32_t sel = 2;
	const uint32_t clkDiv = 15;
	uint32_t pdn = 1;
	uint32_t start = 1;

	const uint32_t doneMask = 1u << 31;
	const uint32_t dataMask = 0x3ff << 6;

	uint16_t answer = 0;

	// Starts the ADC
	AD0CR = sel | (clkDiv << 8) | (pdn << 21) | (start << 24);

	// Wait until done bit is set
	while (!(AD0DR1 & doneMask)) {}

	// Actually reads the data
	answer = (AD0DR1 & dataMask) >> 6;

	// Shuts down the ADC
	pdn = 0;
	start = 0;
	AD0CR = sel | (clkDiv << 8) | (pdn << 21) | (start << 24);

	return answer;
}

/**
 * @brief Saves the data from the sensors, just saves it to an array for now.
 *
 * @param entry The entry to store
 *
 * @return Whether the store was successful
 */
bool saveData(struct DataEntry entry)
{
	// TODO save data to external storage
	// Just saves it to an array for now
	// Volatile ensures the compiler doesn't optimise it out
	static volatile struct DataEntry data[MAX_ENTRIES];
	static int entries;

	if (entries < MAX_ENTRIES) {
		data[entries++] = entry;
		return true;
	}

	return false;
}

/**
 * @brief Initialises the ADC by selecting the correct pin.
 */
void initialiseAdc(void)
{
	// Select AD0[0] pin
	const uint32_t adcClearSelectMask = ~(0x3 << 14);
	const uint32_t adcSelectMask = 0x1 << 14;
	uint32_t pinSel = PINSEL1;
	pinSel &= adcClearSelectMask;
	pinSel |= adcSelectMask;
	PINSEL1 = pinSel;
}

/**
 * @brief Do nothing forever.
 */
void halt(void)
{
	while (true) {};
}
